/**
 * Created by nasir on 27/11/17.
 */

export class PieChartConfig {
  title: string;
  pieHole: number;
  legend: string;

  constructor(title: string, pieHole: number) {
    this.title = title;
    this.pieHole = pieHole;
    // this.legend = 'none';
  }
}
