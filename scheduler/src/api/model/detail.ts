import {IDetailId} from './detailId';
/**
 * Created by nasir on 7/11/17.
 */
export interface IDetail {
  id: IDetailId;
}
