/**
 * Created by nasir on 9/11/17.
 */
export interface IJobLog {
  id: number;
  createdOn: Date;
  logStatement: string;
}
