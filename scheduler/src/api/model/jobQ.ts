import {IDetail} from './detail';
import {IJob} from "./job";
/**
 * Created by nasir on 27/9/17.
 */
export interface IJobQ {
  id: number;
  frequency: string;
  lastRun: Date;
  nextRun: Date;
  repeatTimes: string;
  repeatDuration: string;
  status: string;
  job: IJob;
}
