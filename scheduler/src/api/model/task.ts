/**
 * Created by nasir on 7/11/17.
 */
export interface ITask {
  id: number;
  taskName: string;
  taskType: string;
  taskDesc: string;
}
