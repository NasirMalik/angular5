/**
 * Created by nasir on 27/11/17.
 */

import { Injectable } from '@angular/core';
import { PieChartConfig } from '../config/PieChartConfig';
import { BaseChartsService } from './BaseChartsService';

declare var google: any;

@Injectable()
export class PieChartService extends BaseChartsService {

  constructor() { super(); }

  public buildPieChart(elementId: string, data: any[], config: PieChartConfig) : void {
    var chartFunc = () => { return new google.visualization.PieChart(document.getElementById(elementId)); };
    var options = {
      title: config.title,
      pieHole: 0.2,
      legend: config.legend,
      pieSliceText: 'none',
      slices: {0: {color: '#d0ff8f'},      // Client Assigned
               1: {color: '#fe1e17'},      // Error
               2: {color: '#82df00'},      // Finished
               3: {color: '#e7ac00'},      // Long Overdue
               4: {color: '#b0ff42'},      // Running
               5: {color: '#01e5de'},      // Scheduled
               6: {color: '#95ff02'},      // Task Done
               7: {color: '#ffe391'}}      // Overdue
    };



    this.buildChart(data, chartFunc, options);
  }
}
