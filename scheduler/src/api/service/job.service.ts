import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import {IJob} from '../model/job';
import {IJobQ} from '../model/jobQ';
import {IJobLog} from '../model/jobLog';
import {HttpClient} from '@angular/common/http';
/**
 * Created by nasir on 27/9/17.
 */

@Injectable()
export class JobService {

  serverUrl: string = 'http://localhost:8083';

  constructor(private _http: HttpClient) {  }

  getJobs(): Observable<IJob[]> {
    return this._http.get(this.serverUrl + '/schedule/jobs')
      .map(response => <IJob[]> response);
  }

  getJob(jobId: string): Observable<IJob> {
    return this._http.get(this.serverUrl + '/schedule/jobs/' + jobId)
      .map(response => <IJob> response);
  }

  getJobQ(): Observable<IJobQ[]> {
    return this._http.get(this.serverUrl + '/schedule/jobQ')
      .map(response => <IJobQ[]> response);
  }

  getJobQued(jobQId: string): Observable<IJobQ> {
    return this._http.get(this.serverUrl + '/schedule/jobQ/' + jobQId)
      .map(response => <IJobQ> response);
  }

  getJobsQuedByJobId(jobId: string): Observable<IJobQ[]> {
    return this._http.get(this.serverUrl + '/schedule/jobQ/job/' + jobId)
      .map(response => <IJobQ[]> response);
  }

  getLogs(jobQId: string): Observable<IJobLog[][]> {
    return this._http.get(this.serverUrl + '/schedule/jobQ/log/' + jobQId)
      .map(response => <IJobLog[][]> response);
  }

  getStatus(): Observable<any> {
    return this._http.get(this.serverUrl + '/schedule/jobQ/status');
    // .do(data => console.log(JSON.stringify(data)));
  }

}
