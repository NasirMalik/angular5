import { Component } from '@angular/core';
// import {ProductService} from './products/product.service';
import {JobService} from '../api/service/job.service';
import {PieChartService} from '../api/service/PieChartService';
import {PieChartComponent} from './home/PieChartComponent';

@Component({
  selector: 'pm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [/*ProductService,*/ JobService, PieChartService, PieChartComponent]
})
export class AppComponent { }
