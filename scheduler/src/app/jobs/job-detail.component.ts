import {Component, Input, OnInit} from '@angular/core';
import {IJob} from '../../api/model/job';
import {ActivatedRoute, Router} from '@angular/router';
import {JobService} from '../../api/service/job.service';

@Component({
  templateUrl: './job-detail.component.html',
  styleUrls: ['./job-detail.component.css']
})
export class JobDetailComponent implements OnInit {

  jobId: string;
  pageTitle: string = 'Job Tasks';
  job: IJob;

  constructor(private _route: ActivatedRoute,
              private _service: JobService,
              private _router: Router) {
    this.jobId = this._route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this._service.getJob(this.jobId)
      .subscribe(ijob => {
        this.job = ijob;
      });
  }

  onBack(): void {
    this._router.navigate(['/jobs']);
  }

}
